#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest.h"

#include <iostream>

TEST_SUITE("suite 1") {
    TEST_CASE("hello 1") {
        int x = 2 * 2;
        REQUIRE(x == 4);
    }

    TEST_CASE("hello 2") {
        int x = 3 * 3;
        REQUIRE(x == 9);
    }
}