cmake_minimum_required(VERSION 3.17)
project(hello_doctest)

set(CMAKE_CXX_STANDARD 17)

add_executable(hello_doctest main.cpp)
target_include_directories(hello_doctest PUBLIC
        ${PROJECT_SOURCE_DIR}/deps/doctest/doctest
        )

